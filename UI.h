#ifndef CONNECT4_UI_H
#define CONNECT4_UI_H

#include <string>
#include "board.h"

//
class UI {
public:

    /**
     * Shows a message in some way over the interface.
     * @param msg message to be shown to the user
     * @return reference to the UI
     */
    virtual UI & showMessage(std::string msg) = 0;

    /**
     * #showMessage with the operator <<
     */
    virtual UI & operator<<(std::string) = 0;

    /**
     * Shows a board
     * @param daBoard Board to be displayed
     * @return reference to the UI.
     */
    virtual UI & showBoard(const board& daBoard) = 0;

    /**
     * #showBoard with the operator <<
     */
    virtual UI & operator<<(const board& daBoard) = 0;

    /**
     * Gets a number in some way
     * @param target target to store the number in
     * @return reference to the UI.
     */
    virtual UI & getNumber(int& target) = 0;

    /**
     * #getNumber with the >> operator
     * @return reference to the UI.
     */
    virtual UI & operator>>(int& target) = 0;
};

class consoleUI : public UI{
public:

    /**
     * Shows a message over the console.
     * @param msg message to be shown.
     * @return reference to the UI
     */
    UI & showMessage(std::string msg) override ;

    /**
     * #showMessage with the << operator
     */
    UI & operator<<(std::string string) override ;

    /**
     * Shows the board over the console.
     * @param daBoard the board to show over the console.
     * @return reference to the UI
     */
    UI &showBoard(const board &daBoard) override;

    /**
     * #showBoard with the << operator
     */
    UI &operator<<(const board &daBoard) override;

    /**
     * Gets a number from the console
     * @param target target to store the number in
     * @return reference to the UI
     */
    UI &getNumber(int &target) override;

    /**
     * #getnumber using the >> operator
     */
    UI &operator>>(int &target) override;
};


#endif //CONNECT4_UI_H
