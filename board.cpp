/**
 * See this file for descriptions of each function.
 */
#include <sstream>
#include <iostream>
#include "board.h"

board::board() {
    for(int i = 0; i < 6; i++){
        for(int l = 0; l < 7; l++){
            disBoard[i][l] = 0;
        }
    }

}

board::~board() = default;

void board::setTokens(char t1, char t2) {
    token1 = t1;
    token2 = t2;
}

int board::getSequence(int r, int c, int dr, int dc, int aff) {
    int multiplier = 1;
    int count = 0;
    for(int i = 1; i >= -1; i-=2) {
        while (r + (dr * multiplier * i) >= 0 && r + (dr * multiplier * i) <= 5 &&
               c + (dc * multiplier * i) >= 0 && c + (dc * multiplier * i) <= 6 &&
               disBoard[r + (dr * multiplier * i)][c + (dc * multiplier * i)] == aff) {
            count++;
            multiplier++;
        }
    }
    return count;
}

bool board::checkWin(int x, int y, int aff) {
    bool hasWon = false;
    int dx = -1;
    int dy = -1;
    while(!hasWon && dy <= 0){
        getSequence(x, y, dx, dy, aff) >= 3 ? hasWon = true : dx == 1 ? dy++ : dx++;
    }
    return hasWon;
}

void board::changeAff(int aff, int x, int y) {
    disBoard[x][y] = aff;
}

int board::place(int aff, int y) {
    int x = 5;
    while(disBoard[x][y] != 0 && x >= 0) {
        x--;
    }
    (x >= 0 ? disBoard[x][y] = aff : x = -1);
    return x;
}

std::string board::toString() const{
    std::ostringstream rVal;
    rVal << "  1   2   3   4   5   6   7" << '\n';
    for(int i = 0; i < 6; i++){
        rVal << "| ";
        for(int l = 0; l < 7; l++){
            switch(disBoard[i][l]){
                case 0:
                    rVal << " ";
                    break;
                case 1:
                    rVal << token1;
                    break;
                case -1:
                    rVal << token2;
                    break;
            }
            rVal << " | ";

        }
        rVal << '\n';
    }
    return rVal.str();
}

std::ostream& operator<<(std::ostream &os, const board& daBoard) {
    os << daBoard.toString();
    return os;
}
