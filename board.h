#ifndef CONNECT4_BOARD_H
#define CONNECT4_BOARD_H

#include <string>

/**
 * The board that the game will be played on.
 */
class board {

    /**
     * A character that represents the token of player 1
     */
    char token1;

    /**
     * A character that represents the token of player 2
     */
    char token2;

    /**
     * A 2D array representing the standard connect 4
     * board, or a 6 * 7 matrix. Each cell in the matrix
     * should have 1 of 3 values:
     * 0 = Unowned.
     * 1 = Player 1.
     * -1 = Player 2.
     */
    int disBoard[6][7];

    /**
     * Checks if the given coordinates are a piece
     * of 4 in a row.
     * @param r row coordinate
     * @param c column coordinate
     * @param dr row to search
     * @param dc column to search
     * @param aff affinity to check four a row of
     * @return if the coordinates is part of 4
     */
    int getSequence(int r, int c, int dr, int dc, int aff);

public:

    /**
     * Initializes all elements in disboard to 0.
     */
    board();

    /**
     * BLOWS IT UP
     */
    ~board();

    /**
     * Tells if the player with the given affinity value has won.
     * Should be used after placing a token since that is the only
     * point at which a player can win if they have not already.
     * @param x vertical coordinate
     * @param y horizontal coordinate
     * @param aff player affinity
     * @return
     */
    bool checkWin(int x, int y, int aff);

    /**
     * Sets the values of each game token.
     * @param t1 player one's token
     * @param t2 player two's token
     */
    void setTokens(char t1, char t2);

    /**
     * Changes the affinity of a particular coordinate.
     * @param aff the value to change the piece to
     * @param x vertical coordinate
     * @param y horizontal coordinate
     */
    void changeAff(int aff, int x, int y);

    /**
     * Changes affinity of the lowest cell in y column
     * that has a value of zero to the value of aff
     * @param aff value to change to
     * @param y column to place
     * @return x coord the token was placed, or -1 if unable.
     */
    int place(int aff, int y);

    /**
     * Assembles the board state in a string representation
     * @return string representing the board state
     */
    std::string toString() const;

};

/**
     * Overloads the stream operator.
     * @param os stream
     * @return
     */
std::ostream& operator <<(std::ostream& os, const board& daBoard);


#endif //CONNECT4_BOARD_H

