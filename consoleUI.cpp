#include <iostream>
#include "UI.h"

UI & consoleUI::showMessage(std::string msg){
    std::cout << msg << std::endl;
    return *this;
}

UI & consoleUI::operator<<(std::string string) {
    return showMessage(string);
}

UI &consoleUI::showBoard(const board &daBoard) {
    std::cout << daBoard << std::endl;
    return *this;
}

UI &consoleUI::operator<<(const board &daBoard) {
    return showBoard(daBoard);
}

UI &consoleUI::getNumber(int &target) {
    scanf("%d", &target);
    std::string flush;
    getline(std::cin, flush);
    return *this;

}

UI &consoleUI::operator>>(int &target) {
    return getNumber(target);
}
