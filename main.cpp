#include <iostream>
#include <fstream>
#include <vector>
#include "board.h"
#include "UI.h"

/**
 * Contains text files that correspond to a number
 * input by the user.
 */
std::string lang[] = {"eng.txt","span.txt","jp.txt","de.txt","cm.txt"};

/**
 * Contains the game messages of the selected
 * languages. Populated by #init()
 */
static std::vector<std::string> v_messages;

/**
 * Makes it a little easier to understand what message is being
 * used in certain places.
 */
enum e_messages{
    MSG_welcome, MSG_enterCol, MSG_enterValid, MSG_noSpaces, MSG_winner
};

/**
 * Shows an error message to the user that explains how to use
 * the command line arguments.
 * @param path argv[0]
 */
void showUsage(std::string path){
    std::cerr << "Usage: " << path << "\navailable arguments:\n-config\t\tConfigure user preferences" << std::endl;
}
/**
 * Initializes the values in #v_messages
 */
int init(int selLang){
    int success = 1;
    std::string temp;
    std::ifstream messages("lang/" + lang[selLang]);
    if(!messages){
        std::cout << "Language file is missing or corrupt." << std::endl;
        success = 0;
    }
    while(std::getline(messages,temp)){
        v_messages.push_back(temp);
    }
    return success;
}

/**
 * Prompts the user to input the language that they want to see
 * all messages displayed in.
 * @return integer corresponding to the choices in the menu.
 */
int langSel(){
    std::cout << "1. English\n2. Español\n3. 日本語\n4. De Wei\n5. Caveman" << std::endl;
    int langSel = 0;
    while(langSel > 5 || langSel < 1){
        scanf("%d", &langSel);
        std::string flush;
        getline(std::cin, flush);

    }
    return langSel;
}

/**
 * Prompts the user to enter a token for a certain player
 * @param who player who the token will represent
 * @return character that will represent the character
 */
char tokenSel(const std::string &who){
    char token;
    std::cout << "Enter a character to represent " << who << std::endl;
    scanf("%c", &token);
    std::string flush;
    getline(std::cin, flush);
    while(token <= 32 || token >= 127){
        std::cout << "Enter a non whitespace character!" << std::endl;
        scanf("%c", &token);
        getline(std::cin, flush);
    }
    return token;
}

/**
 * Interprets the config file.
 * If the file does not exist then the program will
 * prompt the user all config options and populate a new config.
 * @return relevant info from config.
 */
//TODO Make the token input and display not shit, work for UTF would be swell as well.
int handleConfig(int setConfig, board* daBoardPtr){
    std::ifstream reader("config.dat");
    int lang = -1;
    char t1 = 'x';
    char t2 = 'o';
    if(!reader || setConfig == 1){
        std::ofstream writer("config.dat");
        lang = langSel()-1;
        t1 = tokenSel("Player One");
        t2 = tokenSel("Player Two");
        writer << lang << std::endl;
        writer << t1 << std::endl;
        writer << t2 << std::endl;
        writer.close();
    }
    else{
        reader >> lang;
        reader >> t1;
        reader >> t2;
        if(lang < 0 || lang > 4){
            std::ofstream writer("config.dat");
            writer << 0;
            writer << 'x' << std::endl;
            writer << 'o' << std::endl;
            writer.close();
            lang = 0;
        }
    }
    daBoardPtr -> setTokens(t1, t2);
    return lang;
}

/**
 * Prompts the player to enter a valid column number.
 * @return selected valid column number.
 */
int getCol(UI* displayPtr) {
    int column = 0;
    *displayPtr << v_messages[MSG_enterCol];
    while (column == 0) {
        *displayPtr >> column;
        if (column <= 0 || column > 7) {
            *displayPtr << v_messages[MSG_enterValid];
            column = 0;
        }
    }
    return column - 1;
};

/**
 * The main game loop, determines the winner of the game by having
 * players place tokens until one of them results in four in a row.
 * @param daBoard board for the players to interact on.
 * @param player player who has the active priority
 * @return the value of the player who won the game.
 */
int gameLoop(board* daBoardPtr, UI* displayPtr, int player){
    int winner = 0;
    while (winner == 0) {
        int row = -1;
        int column = 0;
        while (row == -1) {
            column = getCol(displayPtr);
            row = daBoardPtr -> place(player, column);
            if (row == -1) {
                *displayPtr << v_messages[MSG_noSpaces];
            }
        }
        daBoardPtr -> checkWin(row, column, player) ? winner = player : player *= -1;
        *displayPtr << *daBoardPtr;
    }
    return winner;
}

/**
 * The main method, basically just alternates the player variable between 1
 * and -1 to represent whose turn it is, and then #getcol will be called to prompt
 * that player to place a token. Once a valid destination has been found, the game
 * will determine if the placement of that token resulted in a winner, and if so,
 * end the game loop. The result of game loop indicates the player that has won,
 * 1 = player 1, -1 = player 2.
 */
int main(int argc, char **argv) {
    int setConfig = argc == 2 && (std::string)argv[1] == "-config" ? 1: 0;
    if(argc > 1 && setConfig == 0){
        showUsage(argv[0]);
    }
    UI* displayPtr = new consoleUI;
    board* daBoardPtr = new board();
    if(init(handleConfig(setConfig, daBoardPtr)) != 0) {
        *displayPtr << v_messages[MSG_welcome] << *daBoardPtr;
        char winner = gameLoop(daBoardPtr, displayPtr, 1) == 1 ? '1' : '2';
        *displayPtr << v_messages[MSG_winner] + winner;
    }
    return 0;
}